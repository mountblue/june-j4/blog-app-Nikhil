const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const Schema = mongoose.Schema

app.use(express.static('build'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))

app.get('/', function (req, res) {
    res.render('index')
})

mongoose.connect('mongodb://localhost:27017/blog', {
    useNewUrlParser: true
})

let blogSchema = new Schema({
    _id: Number,
    checked: Boolean,
    title: String,
    body: String
}, {
    autoIndex: false
})

let Blogs = mongoose.model('Blogs', blogSchema)

app.get('/api/blogs', function (req, res) {
    Blogs.find({}, function (err, data) {
        res.send(data)
    })
})

app.post('/api/form', function (req, res) {
    Blogs.create(req.body, function (err, data) {
        if (err) {
            console.log(err)
        } else {
            res.send()
        }
    })
})

app.put('/api/blogs/:id', function (req, res) {
    let _id = (req.params.id)
    let check = (req.body.checked)
    Blogs.findOneAndUpdate({
        _id
    }, {
        $set: {
            checked: check
        }
    }, function (err, data) {
        if (err) {
            console.log(err)
        } else {
            res.send()
        }
    })
})

app.put('/api/form/:id', function (req, res) {
    let _id = (req.params.id)
    let title = req.body.title
    let body = req.body.body
    Blogs.findOneAndUpdate({
        _id
    }, {
        $set: {
            title,
            body
        }
    }, function (err, data) {
        if (err) {
            console.log(err)
        } else {
            res.send()
        }
    })
})

app.delete('/api/blogs/:id', function (req, res) {
    let _id = (req.params.id)
    Blogs.deleteOne({
        _id
    }, function (err, data) {
        if (err) {
            console.log(err)
        } else {
            res.send()
        }
    })
})

app.listen(3000, function () {
    console.log('listening on 3000!')
})