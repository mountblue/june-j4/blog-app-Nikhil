import { connect } from "react-redux";
import React, { Component } from "react";
import { Link } from "react-router-dom";

class List extends Component {
  render() {
    return (
      <ul className="blog-list">
        {this.props.data.map((obj, index) => (
          <li key={index}>
            <input
              type="checkbox"
              checked={obj.checked}
              onChange={() => this.props.readHandler(index, obj)}
            />
            <Link to={`/blogs/${obj._id}`}>{obj.title}</Link>
            <input
              type="button"
              value="x"
              onClick={() => this.props.deleteHandler(index, obj)}
            />
          </li>
        ))}
      </ul>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.data
  };
};

const mapDispatchToProps = dispatch => {
  return {
    readHandler: (index, obj) =>
      dispatch({
        type: "READ",
        payload: index,
        obj
      }),
    deleteHandler: (index, obj) =>
      dispatch({
        type: "DELETE",
        payload: index,
        obj
      })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(List);
