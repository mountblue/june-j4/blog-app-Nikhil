import React, { Component } from "react";
import { connect } from "react-redux";
import "./App.css";
import { BrowserRouter, Route, Link } from "react-router-dom";
import Form from "./Form";
import List from "./List";
import Description from "./Description";

class App extends Component {
  componentDidMount() {
    fetch("/api/blogs", {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    })
      .then(data => data.json())
      .then(req =>
        this.props.dispatch({
          type: "SETSTATE",
          payload: req
        })
      );
  }
  render() {
    return (
      <BrowserRouter>
        <div>
          <nav className="header-wrapper">
            <ul>
              <li>
                <Link to={`/`}>Go to Home</Link>
              </li>
              <li>
                <Link to={`/form`}>Go to form</Link>
              </li>
              <li>
                <Link to={`/blogs`}>Go to blogs</Link>
              </li>
            </ul>
          </nav>
          <hr />
          <Route path="/" render={() => <h1 className="welcome">Welcome</h1>} exact={true} />
          <Route path="/form" component={Form} exact />
          <Route path="/form/:id" component={Form} />
          <Route path="/blogs" component={List} exact />
          <Route path="/blogs/:id" component={Description} />
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.data
  };
};

export default connect(mapStateToProps)(App);
