import React, { Component } from "react";
import { connect } from "react-redux";
import "./App.css";
import { Link } from "react-router-dom";

class Description extends Component {
  render() {
    return (
      <div>
        {this.props.data.map(obj => {
          if (obj._id === Number(this.props.match.params.id)) {
            return (
              <div key={obj._id}>
                <h2>{obj.title}</h2>
                <p>{obj.body}</p>
                <Link to={`/form/${obj._id}`}>
                  <button>Edit this Blog</button>
                </Link>
              </div>
            );
          }
        })}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.data
  };
};

export default connect(mapStateToProps)(Description);
