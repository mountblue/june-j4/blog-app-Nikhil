import React, { Component } from "react";
import { connect } from "react-redux";
import "./App.css";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      body: ""
    };
  }

  componentDidMount() {
    if (this.props.match.params.id !== "") {
      this.props.data.map(obj => {
        if (obj._id === Number(this.props.match.params.id)) {
          this.setState({
            title: obj.title,
            body: obj.body,
            _id: obj._id
          });
        }
      });
    }
  }

  handleClick = () => {
    if (this.state.title !== "" && this.state.body !== "") {
      this.props.dispatch({
        type: "SUBMIT",
        payload: this.state
      });
      this.setState({
        title: "",
        body: ""
      });
    }
  };

  handleTitleChange = e => {
    this.setState({
      title: e.target.value
    });
  };

  handleBodyChange = e => {
    this.setState({
      body: e.target.value
    });
  };

  render() {
    return (
      <div className="form-wrapper">
        <input
          type="text"
          className="input-title"
          onChange={this.handleTitleChange}
          value={this.state.title}
          placeholder="Title"
        />
        <textarea
          cols="30"
          rows="10"
          className="input-body"
          onChange={this.handleBodyChange}
          value={this.state.body}
          placeholder="Description"
        />
        <input
          type="reset"
          value="Submit"
          onClick={() => this.handleClick()}
          className="input-submit"
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.data
  };
};

export default connect(mapStateToProps)(Form);
