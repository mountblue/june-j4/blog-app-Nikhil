const initialState = {
  data: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "SETSTATE":
      return { ...state, data: [...state.data, ...action.payload] };

    case "SUBMIT":
      if (!action.payload._id) {
        let obj = Object.assign(
          {},
          action.payload,
          {
            _id: Number(Math.floor(Math.random() * Date.now()))
          },
          { checked: false }
        );

        fetch("../api/form/", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(obj)
        });

        return {
          ...state,
          data: [...state.data, obj]
        };
      } else {
        fetch(`/api/form/${action.payload._id}`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            title: action.payload.title,
            body: action.payload.body
          })
        });

        let data = [];
        state.data.forEach(obj => {
          if (obj._id !== action.payload._id) {
            data.push(obj);
          } else {
            data.push({
              title: action.payload.title,
              body: action.payload.body,
              _id: obj._id,
              checked: obj.checked
            });
          }
        });
        return {
          ...state,
          data
        };
      }

    case "READ":
      console.log(action.obj);
      fetch(`/api/blogs/${action.obj._id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ checked: !action.obj.checked })
      });

      return {
        ...state,
        data: [
          ...state.data.slice(0, action.payload),
          Object.assign(
            {},
            state.data[action.payload],
            (state.data[action.payload].checked = state.data[action.payload]
              .checked
              ? false
              : true)
          ),
          ...state.data.slice(action.payload + 1)
        ]
      };

    case "DELETE":
      fetch("../api/blogs/" + action.obj._id, {
        method: "DELETE"
      });
      return {
        ...state,
        data: [
          ...state.data.slice(0, action.payload),
          ...state.data.slice(action.payload + 1)
        ]
      };

    default:
      return state;
  }
};

export default reducer;
